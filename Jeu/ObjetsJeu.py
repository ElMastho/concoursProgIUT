__author__ = 'julien'

from . import actions


class Tank:
    def __init__(self, idJoueur, position):
        self.idJoueur = idJoueur
        self.position = position
        self.direction = (0, -1)
        self.nbPoints = 0
        self.nbVies = 1

    def deplace(self):
        """
        :param direction: un couple (dx,dy) de direction du joueur
        :return:
        """
        (x, y) = self.position
        (dx, dy) = self.direction
        self.position = (x + dx, y + dy)

    def get_position_si_deplace(self):
        (x, y) = self.position
        (dx, dy) = self.direction
        return (x + dx, y + dy)

    def get_position(self):
        x, y = self.position
        return (x, y)

    def get_direction(self):
        return self.direction

    def hrotate(self):
        self.direction = {(1, 0): (0, 1), (0, 1): (-1, 0),
                          (-1, 0): (0, -1), (0, -1): (1, 0)}[self.direction]
        return self.direction

    def trotate(self):
        self.direction = {(1, 0): (0, -1), (0, 1): (1, 0),
                          (-1, 0): (0, 1), (0, -1): (-1, 0)}[self.direction]
        return self.direction

    def get_infos(self):
        return (self.idJoueur, self.position, self.direction, self.nbPoints)

    def meurt(self, newPos):
        """
        Meurt et repart de la position newPos
        :param newPos:
        :return: True si son nombre de vies descend en dessous de 0
        """
        self.nbPoints -= 10
        self.nbVies -= 1
        self.position = newPos
        return self.nbVies < 0

    def get_nb_vies(self):
        return nbVies

    def get_nb_points(self):
        return self.nbPoints

    def ajoute_points(self, nbPoints):
        self.nbPoints += nbPoints

    def toDict(self):
        return {"id": self.idJoueur, "position": self.position, "direction": self.direction, "score": self.nbPoints}


class Projectile:
    id = 0

    def __init__(self, pos, direction, idJoueur):
        self.pos = pos
        self.velocity = (direction[0], direction[1])
        self.idJoueur = idJoueur
        self.id = Projectile.id
        Projectile.id += 1

    def update(self):
        self.pos = (self.pos[0] + self.velocity[0],
                    self.pos[1] + self.velocity[1])

    def collide(self, position):
        if position == self.pos:
            return True

    def get_position(self):
        return self.pos

    def get_direction(self):
        return self.velocity

    def __repr__(self):
        return str((self.pos, self.idJoueur))

    def get_infos(self):
        return (self.id, self.pos, self.velocity, self.idJoueur)
