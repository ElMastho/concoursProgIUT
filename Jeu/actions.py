def joueur_connect(id, position, direction):
    return [("joueur", "connect", id, position, direction)]


def joueur_disconnect(id, position, direction):
    return [("joueur", "disconnect", id)]


def joueur_hrotate(id):
    return [("joueur", "hrotate", id)]


def joueur_trotate(id):
    return [("joueur", "trotate", id)]


def joueur_rotate(id, orientation):
    return [("joueur", "rotate", id, orientation)]


def joueur_disconnect(id):
    return [("joueur", "disconnect", id)]


def joueur_die(id):
    return [("joueur", "die", id)]


def joueur_move(id):
    return [("joueur", "move", id)]

def joueur_respawn(id, position):
    return [("joueur", "respawn", id, position)]

def joueur_shoot(idJoueur, idProjectile, position, direction):
    return [("joueur", "shoot", idJoueur, idProjectile, position, direction)]


def joueur_recupere_bonus(idJoueur, position):
    return [("joueur", "recupere_bonus", idJoueur, position)]


def projectile_move(idProjectile, toPosition):
    return [("projectile", "move", idProjectile, toPosition)]


def projectile_explose(idProjectile, position, listeTiles, infos):
    return [("projectile", "explode", idProjectile, position, listeTiles, infos)]


def spectateur_infos(infos):
    return [("spectateur", infos)]

def start():
    return [("start", )]

def infos(informations):
    return [("infos", informations)]


def erreur_info(info):
    return [("erreur", info)]


def verifier_actions(listeActions):

    if not (type(listeActions) == list and all(type(action) == str for action in listeActions)):
        return erreur_info("Ordre mal formaté, ce doit être une liste de chaines")

    mouvements = len([x for x in listeActions if x in [
                     "lrotate", "hrotate", "move"]])
    if mouvements > 1:
        return erreur_info("Uniquement un seul lrotate ou hrotate ou move")
    if listeActions.count("shoot") > 1:
        return erreur_info("Uniquement un shoot")


# Pour le type joueur, action a la forme :
#  idJoueur, action où action est :
#        - (rotate, horraire / trigo)
#        - (move,)
#        - (shoot , idProjectile)
#        - connect
#        - disconnect
#        - die

# Pour le type projectile, action a la forme:
#        - (move,)
#        - (explode, liste de tiles)
