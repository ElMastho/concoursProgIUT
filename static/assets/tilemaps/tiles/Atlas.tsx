<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Atlas" tilewidth="16" tileheight="16" tilecount="400" columns="25">
 <image source="battle_city.png" width="400" height="256"/>
 <tile id="16">
  <properties>
   <property name="cassable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="cassable" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="cassable" type="bool" value="false"/>
  </properties>
 </tile>
</tileset>
