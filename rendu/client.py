#!/usr/bin/env python3
import asyncio
import json
import random
import sys
import argparse


class ClientConcoursProg(asyncio.Protocol):

    def __init__(self, loop):
        self.loop = loop
        self.state = 0
        self.liste_actions = [("move", "shoot"), ("move",),
                              ("move",), ("move",), ("hrotate",)]
        self.monde = {}
        self.idJoueur = 1
        self.j_position = [0,0]
        self.j_direction = [0, 1]
        self.matrix_monde = []
        self.idJoueurs = []

    def connection_made(self, transport):
        self.transport = transport
        print("Connecté")
        self.send_message({"nickname": "pandacorps"})

    def data_received(self, data):
        for d in data.decode().strip().split("\n"):
            self.traite_donnees(json.loads(d))

    def traite_donnees(self, donnees):
        self.refresh_map(donnees)
        if "idJoueur" in donnees:
            self.idJoueur = donnees["idJoueur"]
        if "map" in donnees:
            self.monde = donnees
            print("Nombre d'objets dans la carte :" + str(len(donnees["map"])))
            print("Joueurs :" + str(donnees["joueurs"]))
            for j in donnees["joueurs"]:
                if(j["id"] == self.idJoueur):
                    print("Position :" + str(j["position"]))

        self.IA()

    def connection_lost(self, exc):
        self.loop.stop()

    def send_message(self, message):
        self.transport.write(json.dumps(message).encode() + b'\n')

    def IA(self):
        """
        Si un missile lui arrive sur la gueule il bouge
        Si un ennemi est devant lui il tire.
        S'il y a un mur incassable devant lui il tourne dans le sens ou il y en a pas.
        Choisir entre buter l'ennemi ou prendre le bonus.
        Si l'ennemi est assez loin se dirige vers les bonus sinon vers l'ennemi.
        Parameters
        ----------
        map : type
            Description of parameter `map`.

        Returns
        -------
        type
            Description of returned object.
            Return l'action à effectuer.

        """
        choix = self.analyserMap()
        print(choix)

        if(choix=="bonus"):
            actions = self.action_bonus()
        elif(choix=="combat"):
            actions = self.action_combat()
        elif (choix == "fuir"):
            actions = self.action_fuir()
        else:
            actions = self.action_default()


        self.state = (self.state + 1) % len(self.liste_actions)
        self.send_message(actions)


    def crea_matrix(self, trou, mur, ennemis, j_position, points):
        #0 trous
        #1 murs
        #2 ennemis
        #3 Joueur
        #4 points
        #5 murs pas cassable
        max_col = 0
        max_lig = 0
        for t in trou:
            if(t[0]>max_lig):
                max_lig = t[1] + 1
            if(t[1]>max_col):
                max_col = t[0]

        matrix = []
        for i in range(max_lig):
            ligne = []
            for k in range(max_col):
                if((i==0) or (i==max_lig-1) or (k==0) or (k==max_col-1)):
                    ligne.append(5)
                else:
                    ligne.append(0)
            matrix.append(ligne)

        for m in mur:
            matrix[m[1]][m[0]] = 1

        for e in ennemis:
            matrix[e[1]][e[0]] = 2

        matrix[self.j_position[0]][self.j_position[1]] = 3

        for p in points:
            matrix[p[1]][p[0]] = 4

        return matrix


    def analyserMap(self):
        ennemis = []
        for j in self.monde['joueurs']:
            if(j['id']==self.idJoueur):
                self.j_position = j['position']
                self.j_direction = j['direction']
            else:
                ennemis.append(j['direction'])

        trous = []
        murs = []
        points = []
        for case in self.monde['map']:
            if('cassable' in case):
                if(case['cassable'] == True):
                    murs.append(case['pos'])
                else:
                    trous.append(case['pos'])
            elif('points' in case):
                points.append(case['pos'])

        print(points)
        self.matrix_monde = self.crea_matrix(trous, murs, ennemis, self.j_position, points)

        max_col = 0
        max_lig = 0
        for t in trous:
            if(t[0]>max_lig):
                max_lig = t[0]
            if(t[1]>max_col):
                max_col = t[1]

        hg = (self.j_position[1]+1, self.j_position[0]-1)
        h = (self.j_position[1]+1, self.j_position[0])
        hd = (self.j_position[1]+1, self.j_position[0]+1)
        g = (self.j_position[1], self.j_position[0]-1)
        d = (self.j_position[1], self.j_position[0]+1)
        bg = (self.j_position[1]-1, self.j_position[0]-1)
        b = (self.j_position[1]-1, self.j_position[0])
        bd = (self.j_position[1]-1, self.j_position[0]+1)

        try:
            for i in range(1, max_lig-1):
                for k in range(1, max_col-1):
                    for test in [4, 2]:
                        if(self.matrix_monde[hg[1]][hg[0]]==test or self.matrix_monde[h[1]][h[0]]==test or self.matrix_monde[hd[1]][hd[0]]==test or self.matrix_monde[g[1]][g[0]]==test or \
                        self.matrix_monde[d[1]][d[0]]==test or self.matrix_monde[bg[1]][bg[0]]==test or self.matrix_monde[b[1]][b[0]]==test or self.matrix_monde[bg[1]][bg[0]]==test):
                            if(test==4):
                                return "bonus"
                        elif(test==2):
                            return "bonus"
                            return "combat"
        except Exception as e:
            print(e)

        return "bonus"

    def action_bonus(self):
        distance_min = 200
        point = {}
        for elem in self.monde["map"]:
            if 'points' in elem:
                if self.distance(self.j_position,elem)<distance_min:
                    point = elem
                    distance_min = self.distance(self.j_position,elem)
                if(point['pos']==self.j_position):
                    return self.action_fuir()

        if(point == {}):
            return self.action_fuir()
        return self.deplacer(point['pos'])

    def distance(self, pos, elem):
        return abs(pos[0]-elem['pos'][0])+abs(pos[1]-elem['pos'][1])


    def deplacer(self, point):
        deplacement = []
        print("Je me déplace en position "+str(point[0])+","+str(point[1]))
        if self.j_position[0] > point[0]: # point à gauche
            if self.j_direction == [0, -1] or self.j_direction == [1,0]: # orienté doite ou haut
                deplacement.append("trotate")
                if self.j_direction == [0, -1] and self.matrix_monde[self.j_position[0]][self.j_position[1]-1] == 1:
                    deplacement.append("shoot")

            elif self.j_direction == [0, 1]: # orienté bas
                deplacement.append("hrotate")
                if self.matrix_monde[self.j_position[0]][self.j_position[1]-1] == 1:
                    deplacement.append("shoot")

            else: # orienté gauche
                if self.matrix_monde[self.j_position[0]][self.j_position[1]-1] == 1:
                    deplacement.append("shoot")
                deplacement.append("move")
                if len(deplacement) < 2 and self.matrix_monde[self.j_position[0]][self.j_position[1]-2] == 1:
                    deplacement.append("shoot")


        elif self.j_position[0] < point[0]: # point à droite
            if self.j_direction == [0, -1] or self.j_direction == [-1,0]: # orienté haut ou gauche
                deplacement.append("hrotate")
                if self.j_direction == [0, -1] and self.matrix_monde[self.j_position[0]][self.j_position[1]+1] == 1:
                    deplacement.append("shoot")

            elif self.j_direction == [0, 1]: #orienté bas
                deplacement.append("trotate")
                if self.j_direction == [0, -1] and self.matrix_monde[self.j_position[0]][self.j_position[1]+1] == 1:
                    deplacement.append("shoot")

            else: # orienté droite
                if self.matrix_monde[self.j_position[0]][self.j_position[1]+1] == 1:
                    deplacement.append("shoot")
                deplacement.append("move")
                if len(deplacement) < 2 and self.matrix_monde[self.j_position[0]][self.j_position[1]+2] == 1:
                    deplacement.append("shoot")


        else: # point dans la même colonne
            if self.j_position[1] > point[1]:
                if self.j_direction == [0, 1] or self.j_direction == [1,0]: # orienté doite ou bas
                    deplacement.append("trotate")
                    if self.j_direction == [1, 0] and self.matrix_monde[self.j_position[0]-1][self.j_position[1]] == 1:
                        deplacement.append("shoot")

                elif self.j_direction == [-1, 0]: # orienté gauche
                    deplacement.append("hrotate")
                    if self.matrix_monde[self.j_position[0]-1][self.j_position[1]] == 1:
                        deplacement.append("shoot")

                else: # orienté haut
                    if self.matrix_monde[self.j_position[0]-1][self.j_position[1]] == 1:
                        deplacement.append("shoot")
                    deplacement.append("move")
                    if len(deplacement) < 2 and self.matrix_monde[self.j_position[0]-2][self.j_position[1]] == 1:
                        deplacement.append("shoot")


            elif self.j_position[1] < point[1]:
                if self.j_direction == [0, -1] or self.j_direction == [1,0]: # orienté doite ou haut
                    deplacement.append("hrotate")
                    if self.j_direction == [1, 0] and self.matrix_monde[self.j_position[0]+1][self.j_position[1]] == 1:
                        deplacement.append("shoot")

                elif self.j_direction == [-1, 0]: # orienté gauche
                    deplacement.append("trotate")
                    if self.matrix_monde[self.j_position[0]+1][self.j_position[1]] == 1:
                        deplacement.append("shoot")

                else: # orienté bas
                    if self.matrix_monde[self.j_position[0]+1][self.j_position[1]] == 1:
                        deplacement.append("shoot")
                    deplacement.append("move")
                    if len(deplacement) < 2 and self.matrix_monde[self.j_position[0]+2][self.j_position[1]] == 1:
                        deplacement.append("shoot")

        if(len(deplacement)<2 and "shoot" not in deplacement):
            deplacement.append("shoot")
        return deplacement

    def action_combat(self):
        pos_joueur = []
        direction_joueur = []
        pos_ennemi = []
        if(len(self.monde["joueurs"]) == 1):
            print("Je fuis")
            return self.action_fuir()
        else:
            for elem in self.monde["joueurs"]:
                if(elem["id"] == self.idJoueur):
                    pos_joueur = elem["position"]
                    direction_joueur = elem["direction"]

            for elem2 in self.monde["joueurs"]:
                if(elem2["id"] != self.idJoueur and pos_ennemi == []):
                    pos_ennemi = elem2["position"]
                if( elem2["id"] != self.idJoueur and ((abs(elem2["position"][0]-pos_joueur[0])<abs(pos_ennemi[0]-pos_joueur[0])) and (abs(elem2["position"][1]-pos_joueur[1])<abs(pos_ennemi[1]-pos_joueur[1]))) ):
                    pos_ennemi = elem2["position"]
            print(pos_ennemi)
            return self.deplacer(pos_ennemi)
            res = self.action_fuir()
            res.append("shoot")
        return res

    def action_fuir(self):
        pos = [random.randint(1,len(self.matrix_monde)-2),random.randint(1,len(self.matrix_monde[0])-2)]
        print("position:")
        print(pos)
        return self.deplacer(pos)

    def refresh_map(self, actions):
        # print(self.monde)
        # print(actions)
        if "map" not in actions:
            if len(actions)>0:
                for data in actions:
                    if data[0]=="joueur":
                        if data[1]=="move":
                            for j in self.monde['joueurs']:
                                if(j["id"] == data[2]):
                                    direction = j["direction"]
                                    j["position"][0] += direction[0]
                                    j["position"][1] += direction[1]
                        elif data[1]=="rotate":
                            for j in self.monde['joueurs']:
                                if(j["id"] == data[2]):
                                    j["direction"]=data[3]
                        elif data[1]=="shoot":
                            if data[1] not in self.idJoueurs:
                                self.idJoueurs.append(data[1])
                                self.monde["joueurs"].append({"id":data[1],"position":data[4],"direction":data[5],"score":0})
                            for j in self.monde['joueurs']:
                                if(j["id"] == data[2]):
                                    self.monde['map'].append({"projectile":True,"direction":data[5],"position":data[4], "auteur":data[2], "idprojectile":data[3]})
                        elif data[1]=="recupere_bonus":
                            for b in self.monde['map']:
                                if "points" in b:
                                    if b['pos']==data[3]:
                                        self.monde["map"].remove(b)
                        elif data[1]=="respawn":
                            for j in self.monde['joueurs']:
                                if(j["id"] == data[2]):
                                    j["position"]=data[3]
                        else:
                            pass
                    else:
                        if data[1]=="move":
                            for p in self.monde['map']:
                                if "idprojectile" in p:
                                    if data[2]==p["idprojectile"]:
                                        p["position"][0]=p["position"][0]+(p["direction"][0]*2)
                                        p["position"][1]=p["position"][1]+(p["direction"][1]*2)
                        else:
                            for p in self.monde['map']:
                                if "idprojectile" in p:
                                    if data[2]==p["idprojectile"]:
                                        for info in data[4]:
                                            for case in self.monde['map']:
                                                if "pos" in case:
                                                    if case["pos"]==info:
                                                        self.monde["map"].remove(case)
                                        self.monde["map"].remove(p)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="IA concours de programmation")
    parser.add_argument("hostname", help="Nom ou adresse IP du serveur de jeu")
    parser.add_argument("-p", dest="port", default=8889,
                        type=int, help="Port du serveur auquel se connecter")
    args = parser.parse_args()
    loop = asyncio.get_event_loop()

    coro = loop.create_connection(lambda: ClientConcoursProg(loop),
                                  args.hostname, args.port)

    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()
