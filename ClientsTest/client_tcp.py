import asyncio
import json
import random


class EchoClientProtocol(asyncio.Protocol):

    def __init__(self, loop):
        self.loop = loop
        self.ordres = ["hrotate", "trotate"] + ["move"] * 10

    def connection_made(self, transport):
        self.transport = transport
        print("Connecté")
        transport.write(b'{"nickname":"john"}\n')

    def data_received(self, data_multines):
        for data in data_multines.decode().strip().split("\n"):
                donnees = json.loads(data)
                print('Data received: {!r}'.format(donnees))
                self.loop.create_task(self.send_message(
                    [self.ordres[random.randint(0, len(self.ordres) - 1)], "shoot"]))

    def connection_lost(self, exc):
        self.loop.stop()

    async def send_message(self, message):
        self.transport.write(json.dumps(message).encode())


loop = asyncio.get_event_loop()

coro = loop.create_connection(lambda: EchoClientProtocol(loop),
                              'localhost', 8889)

loop.run_until_complete(coro)
loop.run_forever()
loop.close()
